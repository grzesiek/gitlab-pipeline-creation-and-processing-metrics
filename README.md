# Gitlab Pipeline Creation And Processing Metrics

This README.md is my attempt to document Prometheus metrics that we use on
GitLab.com to monitor pipeline creation, builds queuing and build logs
ingestion processes.

This should make it possible to consume them externally, on a self-managed
instance too, because this documention also contains a lot of feature flags
that should be enabled to make metrics and logs available.

## Pipeline creation

### Advanced Pipeline Creation Logger

The advanced pipeline creation logger is a logging facility that makes use of
an elaborate instrumentation of the pipeline creation chain. There are dozens
of steps, that we've instrumented, to measure the time it takes to accomplish
certain steps, like reading `.gitlab-ci.yml`, parsing configuration, reading
included files, etc.

There are a few dozens of instrumented code blocks. The logger will aggregate
the metrics, and create a single log entry for each pipeline creation, if it
exceeds logging thresholds.

Currently, logging threasholds are defined in the [pipeline creation service](https://gitlab.com/gitlab-org/gitlab/-/blob/c3d4a6bf6da2750a5a8a57505c2b2a0ea7327d16/app/services/ci/create_pipeline_service.rb#L9):

```ruby
LOG_MAX_DURATION_THRESHOLD = 3.seconds   # maximum time completion of each the steps can take
LOG_MAX_PIPELINE_SIZE = 2_000            # maximum pipeline size (number of jobs)
LOG_MAX_CREATION_THRESHOLD = 20.seconds  # maximum time a whole pipeline creation can take
```

When any of the thresholds gets exceeded, a log entry will be pushed into
"rails" or "sidekiq" logs.

This feature is hidden behind a feature flag. You can enable it globally by
toggling it:

```ruby
Feature.enable(:ci_pipeline_creation_logger)
```

## Pipeline creation: Prometheus metrics

Below you can find a few metrics that may be helpful to understand pipeline
creation performance. Please see screenshots that attempt to demonstrate how
such metrics can potentially look like. Screenshots are only examples.

### Rate of pipelines created

`sum(rate(pipelines_created_total[1m]))`

### Pipeline creation duration histogram p90

`histogram_quantile(0.90,sum(rate(gitlab_ci_pipeline_creation_duration_seconds_bucket[1m])) by (le))`

![pipeline creation duration](examples/pipeline_creation_duration_1.png)

### Pipeline creation steps duration histogram

To enable this metric toggle a feature flag:

```ruby
Feature.enable(:ci_pipeline_creation_step_duration_tracking)
```

`histogram_quantile(0.90, sum by (step, le) (pipeline_creation_step_duration_bucket:le_step:rate1m))`

![pipeline creation steps histogram](examples/pipeline_creation_step_duration_1.png)

![pipeline creation steps histogram](examples/pipeline_creation_step_duration_2.png)

## Builds queuing: Prometheus metrics

### Runner metrics

Metrics below are being exposed by Runners. Prometheus needs to scrape them,
what is not enabled by default and requires some amount of manual setup.

#### Current value of a runner instance's concurrency

`sum by(job, instance) (gitlab_runner_concurrent)`

#### Current saturation based on concurrency defined per runner instance

`sum by(job, instance) (gitlab_runner_jobs) / sum by(job, instance) (gitlab_runner_concurrent)`

#### Current value of a runner's processing limit

`sum by(job, instance, runner) (gitlab_runner_limit)`

#### Current saturation based on a runner's processing limit

`sum by(job, instance, runner) (gitlab_runner_jobs) / sum by(job, instance, runner) (gitlab_runner_limit)`

#### Current value of runner's `request_concurrency`

`sum by(job, instance, runner) (gitlab_runner_request_concurrency)`

#### Current rate of exceeded request concurrency

`sum by(job, instance, runner) (increase(gitlab_runner_request_concurrency_exceeded_total[5m]))`

#### Current rate of started builds

`sum by (job, instance, runner) (increase(gitlab_runner_jobs_total[5m]))`

#### Histograms of build durations in minutes

`histogram_quantile(0.5, sum by(job, instance, runner, le) (increase(gitlab_runner_job_duration_seconds_bucket[5m]))) / 60`

`histogram_quantile(0.9, sum by(job, instance, runner, le) (increase(gitlab_runner_job_duration_seconds_bucket[5m]))) / 60`

`histogram_quantile(0.99, sum by(job, instance, runner, le) (increase(gitlab_runner_job_duration_seconds_bucket[5m]))) / 60`

#### Runner API requests against GitLab statuses

`sum by(job, instance, runner, status) (increase(gitlab_runner_api_request_statuses_total[5m]))`

### GitLab Rails side Prometheus metrics

These metrics come from instrumentation on the GitLab Rails side. In most cases
there are enabled by default, and if not, then only toggling a feature flag is
required to expose them for scraping.

In order to enable build queuing metrics, the following feature flag needs to
be enabled:

```ruby
Feature.enable(:gitlab_ci_builds_queuing_metrics)
```

#### Builds queue size per runner type histogram

`histogram_quantile(0.99, sum by (le, runner_type) (rate(gitlab_ci_queue_size_total_bucket[5m])))`

![queue size](examples/queue_size_1.png)

#### Duration of the builds queuing SQL queries histogram

`histogram_quantile(0.90, sum by (le, runner_type) (rate(gitlab_ci_queue_retrieval_duration_seconds_bucket[5m])))`

![queue calculation duration](examples/queue_time_1.png)

#### Queue depth histogram

`histogram_quantile(\n  0.99, sum(rate(gitlab_ci_queue_depth_total_bucket[1m])) by (le, queue))`

![queue depth](examples/queue_depth_1.png)

## Build logs ingestion

All build logs ingestion metrics are enabled by default. There is no need to
toggle a feature flag to make them available at the Prometheus' scraping
endpoint.

### Build logs: all errors rate

`sum(rate(gitlab_ci_build_trace_errors_total[1m])) by (error_reason)`

![build log errors metrics](examples/build_logs_errors_1.png
